const mongoose = require('mongoose')

const ProfileSchema = new mongoose.Schema({
    information:{
        type: String,
        required:true
    },
    adress:{
        type: String,
        required:true
    },
    mobile:{
        type: Number,
        required:true
    },
    profileImage:{
        type: Buffer,
        required:false,
    },
    profileImageType:{
        type: String,
        required:false,
    },
    user: {
        type: mongoose.Schema.Types.ObjectId, 
        required: true,
        ref: 'User' //სახელი აუცილებლად უნდა ემთხვეოდეს ავტორის მოდელში გაწერილ სახელს
    }, 
    friends: [
        {
            type: mongoose.Schema.Types.ObjectId, 
            required: true,
            ref: 'User' //სახელი აუცილებლად უნდა ემთხვეოდეს ავტორის მოდელში გაწერილ სახელს
        }
    ],
});

ProfileSchema.virtual('profileImagePath').get(function(){  // იმისთვის რომ იმიჯ ფასი შეიცვალოს და შიგნით ჩაიწეროს ის პასი რომელიც ბაზაში იქნება და ამით შემდეგ გამოვაჩინოთ ჰტმლ ში
    if(this.profileImage !=null && this.profileImageType != null)
    {
        return `data:${this.profileImageType}; charset=utf8; base64, ${this.profileImage.toString('base64')}`
    }
})

const Profile = mongoose.model('Profile',ProfileSchema)
module.exports = Profile;