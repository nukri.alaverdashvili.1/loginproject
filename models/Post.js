const mongoose = require('mongoose')

const PostSchema = new mongoose.Schema({
    description:{
        type: String,
        required:true
    },
    postImage:{
        type: Buffer,
        required:true,
    },
    postImageType:{
        type: String,
        required:true,
    },
    profile: {
        type: mongoose.Schema.Types.ObjectId, 
        required: true,
        ref: 'Profile' //სახელი აუცილებლად უნდა ემთხვეოდეს ავტორის მოდელში გაწერილ სახელს
    }
});

PostSchema.virtual('postImagePath').get(function(){  // იმისთვის რომ იმიჯ ფასი შეიცვალოს და შიგნით ჩაიწეროს ის პასი რომელიც ბაზაში იქნება და ამით შემდეგ გამოვაჩინოთ ჰტმლ ში
    if(this.postImage !=null && this.postImageType != null)
    {
        return `data:${this.postImageType}; charset=utf8; base64, ${this.postImage.toString('base64')}`
    }
})


const Post = mongoose.model('Post',PostSchema)
module.exports = Post;