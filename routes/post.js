const express = require('express')
const router = express.Router();
const path = require('path') 
const { ensureAuthenticated} =require('../config/auth'); //შემოგვაქვს კონფიგიდან ფუნქცია რომელიც უზრუნველყოფს დაადასტუროს არის თუარა ავტორიზებული მომხმარებელი
//models
const User = require('../models/User')
const Post = require('../models/Post')

//Create Form
router.get('/create', ensureAuthenticated, async (req,res)=>{
    res.render('post/create',{post:Post})
})
//create Handle
router.post('/create', ensureAuthenticated, async (req,res)=>{
    const user = await User.findById(req.user.id).populate('profile').exec()
    const newpost = new Post({
        description:req.body.description,
        profile:user.profile.id
    })
    saveCover(newpost, req.body.postImage)
    try{
        await newpost.save()
        res.redirect('/profile')
    }catch(err){console.log(err)}
})

router.delete('/delete/:id', async (req,res)=> {
    let post
    try{
        post = await Post.findById(req.params.id)
        await post.remove()  
         res.redirect(`/profile`)
    }catch{
            res.redirect('/profile')
    }
})

function saveCover(newpost, reqImage) // coverEncoded არის ფაილი რომელიც ინფუთიდან მოდის ანუ ფაილი რომელიც აიტვირთება
{
    if(reqImage == null) {return} 
    const postImage = JSON.parse(reqImage)

    if(postImage != null )
    {
        newpost.postImage = new Buffer.from(postImage.data, 'base64') // ვინახავთ როგორც ბაფერს
        newpost.postImageType = postImage.type //ვინახავს ამ იმიჯის ტაიპს ანუ jpg/png  თუ რომელიც არის
    }
}

module.exports = router;

