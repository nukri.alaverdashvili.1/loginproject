const express = require('express')
const router = express.Router();
const bcrypt = require('bcryptjs')
const passport = require('passport')
const { ensureNotAuthenticated} =require('../config/notauth'); //შემოგვაქვს კონფიგიდან ფუნქცია რომელიც უზრუნველყოფს დაადასტუროს არის თუარა ავტორიზებული მომხმარებელი

//user Model
const User = require('../models/User')
const Profile = require('../models/Profile')

//Login Page
router.get('/login', ensureNotAuthenticated, (req,res)=>{
    res.render('login')
})

//Register Page
router.get('/register', ensureNotAuthenticated, (req,res)=>{
    res.render('register')
})

//Register Page
router.post('/register',(req,res)=>{
    const {name, email, password, password2}= req.body;
    let errors = [];
    //Check Required Fields
    if(!name || !email || !password || !password2)
    {
        errors.push({msg:'Please Fill in all fields'})
    }

    //Check Passwords Match
    if(password !== password2){
        errors.push({msg:'Password do not match'})
    }

    //Check pass Length
    if(password.length < 6){
        errors.push({msg:'Password should be at least 6 charachters'})
    }

    if(errors.length >0){
        res.render('register',{
            errors,
            name,
            email,
            password,
            password2
        })
    }else{
        User.findOne({email:email}) //ვამოწმებტ ბაზასი არსებობს თუარა ის მეილი რომელიც მომხმარებლმა შეიყვანა
        .then(user=>{
            if(user){
                errors.push({msg:'Email is Already registered'}) //თუ არსებობს მაშინ ვატყობინებთ რომ უკვე არსებობს ამ მეილზე
                res.render('register',{
                    errors,
                    name,
                    email,
                    password,
                    password2
                });
            }else{
                const newUser = new User({ // თუ მეილი არ არსებობს მაშინ ახალ იუზერსი ვინახავთ ყველა მონაცემს
                    name,
                    email,
                    password
                });
                
                //Hash Password
                bcrypt.genSalt(10,(err,salt)=> //მაღალი თავდაცვისთვის ჯერ სალტის კოდს ადებს ჩვენს პაროლს
                bcrypt.hash(newUser.password, salt,async (err,hash)=>{ //შემდეგ ჰაშავს ამ პასვორდს
                    if(err) throw err;
                    // Set Password to hashed
                    newUser.password = hash; //ჩვეულებრივი პაროლის ნაცვლად ვიყენებთ დაჰაშულ პასვორდს
                    //saved User
                    newUser.save() //ვინახავთ ახალ იუზერს
                    //saved new profile
                    const newProfile = new Profile({
                        information:`Personal information isnt't available ${newUser.name}`,
                        adress: 'Not Available',
                        mobile: 0157,
                        profileImage: '',
                        profileImageType:'',
                        user:newUser.id
                    })
                    newProfile.save()
                    //update Profile
                    const updateUser = await User.findById(newUser.id)
                    updateUser.profile = newProfile.id
                    await updateUser.save()
                    .then(user=>{
                        req.flash('success_msg', 'You are now registered and can log in') //გადამისამარ»თების შემდეგ ამ მესიჯს გაგზავნის
                        res.redirect('/users/login'); // ვამისამართებთ ლოგინ ფეიჯზე თუ შეინახა იუზერი
                    })
                    .catch(err=>console.log(err));

                }))
            }
        })
    }

})

//login Handle
router.post('/login',(req,res,next)=>{
    passport.authenticate('local',{
        successRedirect:'/profile',
        failureRedirect:'/users/login',
        failureFlash:true
    })(req,res,next);
});

//logout Handle
router.get('/logout',(req,res)=>{
    req.logout();
    req.flash('success_msg','You are logged out')
    res.redirect('/users/login')
})

module.exports = router;

