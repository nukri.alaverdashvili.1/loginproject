const express = require('express')
const router = express.Router();
const { ensureAuthenticated} =require('../config/auth'); //შემოგვაქვს კონფიგიდან ფუნქცია რომელიც უზრუნველყოფს დაადასტუროს არის თუარა ავტორიზებული მომხმარებელი
const User = require('../models/User')
const Post = require('../models/Post')
const Profile = require('../models/Profile');
const { compile } = require('ejs');


//Index Page
router.get('/', (req,res)=>{
    res.render('welcome')
})

//Profle Page
router.get('/profile', ensureAuthenticated, async (req,res)=>{
    const user = await User.findById(req.user.id).populate('profile').exec()
    const post = await Post.find({profile:req.user.profile})
    res.render('profile',{user:user, post:post})
})

// newsFeed Page
router.get('/newsfeed', ensureAuthenticated, async (req,res)=>{
    const post = await Post.find({profile:{$nin:[req.user.profile]}}).populate('profile').exec()
    res.render('newsfeed',{post:post})
})

//other profiles
router.get('/other/profiles', ensureAuthenticated, async (req,res)=>{
    const myProfile = await Profile.find({user:{$in:[req.user.id]}},{friends:1}) // ჩემი პროფილის ნახვა და მეგობრები რომელიც ამ პროფილს ყავს გამოწერილი
    let friends = myProfile[0].friends // შევინახოთ ცვლადში ეს მეგობრების პროფილები
    friends.push(req.user.id) // დავამატოთ ჩვენი პროფილის აიდი
    const profile = await Profile.find({user:{$nin:friends}}).populate('user').exec() //მივიღოთ ის პროფილები რომელიც არ მყავს დამატებული
    if(profile.length >0){
        res.render('otherprofiles',{profile:profile})
    }
    req.flash('error_msg', 'No more other profiles') 
    res.redirect('/profile')
})

//change Profile
router.get('/profile/change',ensureAuthenticated, async (req,res)=>{
        const user = await User.findById(req.user.id).populate('profile').exec()
        res.render('profile/change',{user:user})   
})

//add Friend 
router.get('/addfriend/:id',ensureAuthenticated, async (req,res)=>{
    const profile = await Profile.find({user:{$in:[req.user.id]}})
    const check = profile[0].friends.find(friends=>{
        return friends == req.params.id
    })
    if(!check)
    {
        await Profile.updateOne({user:req.user.id},{$push:{friends:req.params.id}})
        res.redirect('/other/profiles')
    }
    //status
    res.redirect('/friends')
})

//friends

router.get('/friends',ensureAuthenticated, async (req,res)=>{
        const myProfile = await Profile.find({user:{$in:[req.user.id]}},{friends:1})
        const friendProfile = await Profile.find({user:{$in:myProfile[0].friends}}).populate('user').exec()
        if(friendProfile.length >0){
            res.render('profile/friends',{friendProfile:friendProfile})
        }
        req.flash('error_msg', 'You havent friends') 
        res.redirect('/profile')

})

//Change Profile Handle
router.put('/profile/change', ensureAuthenticated, async (req,res)=>{
    try{
        const profile = await Profile.find({user:req.user.id})
        profile[0].information = req.body.information
        profile[0].adress = req.body.adress
        profile[0].mobile = req.body.mobile
    
        if(req.body.profileImage != null && req.body.profileImage !== '')
        {
            saveCover(profile[0], req.body.profileImage)
        }
        await profile[0].save()
        res.redirect('/profile')

    }catch(err){
        res.redirect('/profile')
    }
})

function saveCover(profile, reqImage) // coverEncoded არის ფაილი რომელიც ინფუთიდან მოდის ანუ ფაილი რომელიც აიტვირთება
{
    if(reqImage == null) {return} 
    const profileImage = JSON.parse(reqImage)
    if(profileImage != null)
    {
        profile.profileImage = new Buffer.from(profileImage.data, 'base64') // ვინახავთ როგორც ბაფერს
        profile.profileImageType = profileImage.type //ვინახავს ამ იმიჯის ტაიპს ანუ jpg/png  თუ რომელიც არის
    }
}
module.exports = router;

