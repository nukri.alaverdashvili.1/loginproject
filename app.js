const express = require('express')
const app = express()
const expressLayouts = require('express-ejs-layouts')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const methodOverride = require('method-override') //საჭიროა მეთოდის გადასაწერად, მაგალითად ფუთ და დელეთე მეთოდის გამოსაძახებლად
const flash = require('connect-flash')
const session = require('express-session')
const passport = require('passport') 
const dotenv = require('dotenv')

//passport Config 
require('./config/passport')(passport); //config ფაილიდან შემოგვაქვს პასპორტი

//DB config 
dotenv.config();
const PORT = process.env.PORT;

//Connect to Mongo
mongoose.connect(process.env.DB_CONNECT, {useNewUrlParser:true}) //დაკავშირება მონგოსთა, userNewUrlParser გვჭირდება ახალი ვერსიისთვის, თუ არ გავუწერთ ერორები გვექნება
.then(()=>console.log('Mongodb Connected...')) // გამოაქვს შეტყობინება წარმატებული დაკავშირების შემდეგ
.catch(err=>console.log(err)); //ერორის სემთხვევაში დაბეჭდოს ეს ერორი

//Ejs
app.use(expressLayouts)
app.set('view engine', 'ejs') 
app.set('views', __dirname +'/views')
app.set('layout', 'layout') 
app.use(express.static('public')) // ამას როდესაც ვუთითებთ სტატიკურად მთავარ ფოლდერს პაბლიქს გვიჩვენებს
app.use(methodOverride('_method'))

//bodyparser
app.use(bodyParser.urlencoded({ limit:'25000mb', extended:false})) //ამის დახმარებით მივიღებთ req.body დან მონაცემებს

//express session middlware
app.use(session({
    secret:'Secret',
    resave:true,
    saveUninitialized:true,
}))
//passport middlware
app.use(passport.initialize());
app.use(passport.session());

// Connect Flash გადამისამართების შემდეგ შეტყობინების გამოსატანად გვჭირდება ფლეში
app.use(flash())

//Global vars
app.use((req,res,next)=>{
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    next();
})

//Routes
app.use('/', require('./routes/index'))
app.use('/users', require('./routes/users'))
app.use('/users/post', require('./routes/post'))

app.listen(PORT, console.log(`Server started on port ${PORT}`))
