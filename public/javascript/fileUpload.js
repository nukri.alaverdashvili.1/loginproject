FilePond.registerPlugin(
   FilePondPluginImagePreview,
   FilePondPluginImageResize,
   FilePondPluginFileEncode,
   FilePondPluginFileValidateType,
   FilePondPluginImageCrop,
)
FilePond.create(document.querySelector('input[type="file"]'),{
   labelIdle: `Drag & Drop your picture or <span class="filepond--label-action">Browse</span>`,
   allowImageResize:true,
   imagePreviewHeight: 170,
   imageCropAspectRatio: '1:1',
   imageResizeTargetWidth: 200,
   imageResizeTargetHeight: 200,
   stylePanelLayout: 'compact circle',
   styleLoadIndicatorPosition: 'center bottom',
   styleProgressIndicatorPosition: 'right bottom',
   styleButtonRemoveItemPosition: 'left bottom',
   styleButtonProcessItemPosition: 'right bottom',
 });
FilePond.parse(document.body);
